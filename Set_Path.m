

mypath   = 'C:\xxx\xxx';% write here the complete path toward your folder workshop_DCM 


%% Set Paths for the TUTORIAL DCM 
%----------------------------------
PathTUTO = fullfile(mypath,'workshop_DCM'); 

restoredefaultpath;

addpath(fullfile(PathTUTO,'spm12'));
spm('defaults', 'eeg'); % set paths for MEEG SPM12

% Add folder
addpath(fullfile(PathTUTO,'data_SPM_EEG_mmn'));

% Go to folder
cd(fullfile(PathTUTO,'data_SPM_EEG_mmn'));

