function DO_ft2spm(cfg,data)
% OBOB_FT2SPM converts Fieldtrip data structures to SPM file format
%
% Use this function as:
%   spmdata = obob_ft2spm(cfg,data)
%
%  data  = fieldtrip dataset structure
%
% The configuration has the following options:
%---------------------------------------------
%
%  cfg.spmfilename = string with full path and filename of output SPM matfile (.mat)
%  cfg.sensfile    = string with full path and filename of file where sensors information are stored
%                    (will be read by ft_read_sens and ft_read_headshape)
%  cfg.senstype    = 'MEG' or 'EEG'
%  cfg.condlabels  = celle array of conditions labels (must be same number of trials)
%  cfg.LFP         = 'yes' or 'no' if your data are virtual sensor sources extraction

%% do some initialization...
%----------------------------

% set the defaults
%------------------
if numel(cfg.condlabels) ~= numel(data.trial)
  error('cfg.condlabels dimensions dont match with trials number');
end

condlabels = ft_getopt(cfg, 'condlabels', num2cell(1:numel(data.trial)));

%% Convert to SPM format
%------------------------
D = spm_eeg_ft2spm(data, cfg.spmfilename);

%% Set condition labels
%-------------------------
for c = 1:numel(condlabels)
  D = conditions(D, c, condlabels{c});
end

%% Post-processing
%------------------
if strcmp(cfg.LFP, 'yes')
  % set channels labels to LFP
  %-----------------------------
  D = chantype(D,1:nchannels(D),'LFP');
  
elseif strcmp(cfg.LFP, 'no')      
  
  % Read sensors and fiducials from cfg.sensfile
  %----------------------------------------------------
  switch cfg.senstype
    case 'EEG'
      
      D = sensors(D, cfg.senstype, ft_convert_units(ft_read_sens(cfg.sensfile,'senstype',cfg.senstype), 'mm'));
      D = fiducials(D, ft_convert_units(ft_read_headshape(cfg.sensfile,'senstype',cfg.senstype), 'mm'));
      
      S = [];
      S.task = 'project3D';
      S.modality = 'EEG';
      S.updatehistory = 1;
      S.D = D;
      D = spm_eeg_prep(S);
      
    case 'MEG'
      S = [];
      S.task = 'loadmegsens';
      S.source = cfg.sensfile;
      S.D = D;
      D = spm_eeg_prep(S);
      
  end
else
  error('cfg.LFP should be ''yes'' or ''no''');
end


%% Save the dataset
%--------------------
save(D);

if strcmp(cfg.LFP, 'no')
  
  %% Compute the forward model if needed
  %--------------------------------------
  D = spm_eeg_load(fullfile(D.path, D.fname));
  
  S = [];
  S.task = 'coregister';
  S.D = D;
  D = spm_eeg_prep(S);

  switch cfg.senstype
    case 'EEG'
      D.inv{1}.forward(1).voltype = 'EEG BEM';
    case 'MEG'
      D.inv{1}.forward(1).voltype = 'Single Shell';
  end
  
  D = spm_eeg_inv_forward(D, 1);
  save(D);
  
end


